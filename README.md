# Release Image

`release-image` automates versioning, CHANGELOG generation, and Docker image tag generation using [semantic versioning](https://semver.org/) and [conventional commit messages](https://www.conventionalcommits.org/). It builds on top of [standard-version](https://github.com/conventional-changelog/standard-version) (specifically, [detouched/standard-version Docker image](https://hub.docker.com/r/detouched/standard-version)).

## Quick start

In the root of your project, define the following variables in a .env file (remember to add .env to your .gitignore).

```.env
# template.env

# Use this file to start a new .env file in the root of your
# project, or copy its contents into an existing .env.
# And then edit these values to suit your personal environment
# and project.
#
# Be sure to add .env to your project's .gitignore.

# To run Docker commands
# Make sure the values are correct for your system.
DOCKER_SOCK=/var/run/docker.sock
DOCKER_EXE=/usr/local/bin/docker

# To make git commits and tags
# Your name and email.
GIT_AUTHOR_NAME=
EMAIL=
```

In the root of your project, add a `release-image.yml` file with the contents below. You may want to change `latest` to a specific version of release-image.

```yml
version: "3.8"
services:
  release-image:
    image: registry.gitlab.com/librefoodpantry/spikeathons/lfp-spikeathon-summer-2020/release-image:latest
    environment:
      # Space delimited list of tags that reference images in your
      # local Docker environment to be released. These tags must encode
      # the repository where the referenced image will ultimately be
      # published.
      # e.g., registry/user/image:build registry/user/image2:build .
      - IMAGES_TO_RELEASE=SPACE_DELIMITED_LIST_OF_IMAGES

      # Leave these blank; they are set in .env; we just need to pass them through
      - GIT_AUTHOR_NAME
      - EMAIL
    volumes:
      - "${DOCKER_SOCK}:/var/run/docker.sock:ro"
      - "${DOCKER_EXE}:/usr/local/bin/docker:ro"
      - "${PWD}:/app"
```

Build and test your project's images and tag them with the same value you set IMAGES_TO_RELEASE.

Now cut a release.

```bash
docker-compose -f release-image.yml run --rm release-image
```

Run the commands printed by the previous command when you are ready to publish your release.
