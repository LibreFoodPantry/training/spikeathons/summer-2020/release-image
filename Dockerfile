FROM detouched/standard-version:8.0.0
RUN apk add --no-cache bash
ENTRYPOINT [ "entrypoint.bash" ]
COPY entrypoint.bash /bin
