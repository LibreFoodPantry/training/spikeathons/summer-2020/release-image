#!/bin/bash

set -xue

main(){
  standard-version "$@"
  VERSION="$(git describe --abbrev=0)"
  VERSION_1_2_3="${VERSION#v}"
  VERSION_1_2="${VERSION_1_2_3%.*}"
  VERSION_1="${VERSION_1_2%.*}"

  PUBLISH_COMMANDS="git push --follow-tags origin master\n"

  IFS=' ' read -ra IMAGES <<< "$IMAGES_TO_RELEASE"
  for IMAGE in "${IMAGES[@]}"; do
    BASE="${IMAGE%:*}"
    docker tag "${IMAGE}" "${BASE}:${VERSION_1_2_3}"
    docker tag "${IMAGE}" "${BASE}:${VERSION_1_2}"
    docker tag "${IMAGE}" "${BASE}:${VERSION_1}"
    docker tag "${IMAGE}" "${BASE}:latest"
    PUBLISH_COMMANDS="${PUBLISH_COMMANDS}docker push ${BASE}:${VERSION_1_2_3}\ndocker push ${BASE}:${VERSION_1_2}\ndocker push ${BASE}:${VERSION_1}\n"
  done

  printf '\n\nWhen you are ready to publish the release, run the following commands:\n\n%b\n\n' "${PUBLISH_COMMANDS}"

}

main "$@"
