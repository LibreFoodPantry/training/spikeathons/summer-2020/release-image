# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.1](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release-image/compare/v2.0.0...v2.0.1) (2020-06-11)

## [2.0.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release-image/compare/v1.1.0...v2.0.0) (2020-06-11)


### ⚠ BREAKING CHANGES

* Replaces IMAGE_TO_RELEASE environment variable
with IMAGES_TO_RELEASE (note the plural "s"). This new variable
can be set to a list of images sepearated by a space.

### Features

* Support release of 0 or more images ([2625170](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release-image/commit/26251705c54a0931fbbc4399a4265e8c6cb6770a))

## [1.1.0](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.3...v1.1.0) (2020-06-09)


### Features

* Don't have to clone to use ([b550688](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/commit/b550688abc236711a4ee73e67f7bc6a8b1ef262d))

### [1.0.4](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.3...v1.0.4) (2020-06-09)

### [1.0.3](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.2...v1.0.3) (2020-06-09)

### [1.0.3](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.2...v1.0.3) (2020-06-09)

### [1.0.2](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.1...v1.0.2) (2020-06-09)

### [1.0.2](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.1...v1.0.2) (2020-06-09)

### [1.0.1](https://gitlab.com/LibreFoodPantry/spikeathons/lfp-spikeathon-summer-2020/release/compare/v1.0.0...v1.0.1) (2020-06-06)

## 1.0.0 (2020-06-06)
